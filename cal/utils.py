from datetime import datetime, timedelta, date
from calendar import HTMLCalendar
import calendar
import pprint
from .models import Event


def changeDayName(day):
	if day == 'Monday':
		return 'Даваа'

class Calendar(HTMLCalendar):

	cssclasses = ["Даваа", "Мягмар", "Лхагва", "Пүрэв", "Баасан", "Бямба", "Ням"]
	cssmonths = ['1-сар', '2-сар', '3-сар', '4-сар', '5-сар', '6-сар', '7-сар', '8-сар', '9-сар', '10-сар', '11-сар', '12-сар']
	today = date.today()
	today_year = int(today.strftime("%Y"))
	today_month = int(today.strftime("%m"))
	today_day = int(today.strftime("%d"))
	def formatweekday(self, day):
		return '<th class="%s">%s</th>' % (calendar.day_abbr[day], self.cssclasses[day])

	def formatweekheader(self):
		s = ''.join(self.formatweekday(i) for i in self.iterweekdays())
		return '<tr>%s</tr>' % s

	def formatmonthname(self, theyear, themonth, withyear=True):
		if withyear:
			s = '%s %s' % (self.cssmonths[themonth-1], theyear)
		else:
			s = '%s' % month_name[themonth]
		return '<tr class="header"><th colspan="7" class="month">%s</th></tr>' % s

	def __init__(self, year=None, month=None):
		self.year = year
		self.month = month
		super(Calendar, self).__init__()

	# formats a day as a td
	# filter events by day
	def formatday(self, day, events):
		events_per_day = events.filter(start_time__day=day)
		d = ''
		for event in events_per_day:
			d += f'<li> {changeDayName(event.title)} </li>'

		pprint.pprint(self.today_year)
		pprint.pprint(self.month)
		if day != 0:
			if self.today_day == day and self.year == self.today_year and self.month == self.today_month:
				return f"<td><span class='date current'>{day}</span></td>"
			else:
				return f"<td><span class='date'>{day}</span><ul> {d} </ul></td>"
		return '<td></td>'

	# formats a week as a tr
	def formatweek(self, theweek, events):
		week = ''
		for d, weekday in theweek:
			week += self.formatday(d, events)
		return f'<tr> {week} </tr>'

	# formats a month as a table
	# filter events by year and month
	def formatmonth(self, withyear=True):
		events = Event.objects.filter(start_time__year=self.year, start_time__month=self.month)

		cal = f'<table border="0" cellpadding="0" cellspacing="0" class="calendar">\n'
		cal += f'{self.formatmonthname(self.year, self.month, withyear=withyear)}\n'
		cal += f'{self.formatweekheader()}\n'
		for week in self.monthdays2calendar(self.year, self.month):
			cal += f'{self.formatweek(week, events)}\n'
		return cal
